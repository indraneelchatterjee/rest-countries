/* eslint-disable react/prop-types */
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon } from "@fortawesome/free-solid-svg-icons";
const NavBar = ({ isDarkMode, themeChange }) => {
  return (
    <>
      <div className={`navbar-container ${isDarkMode ? "dark" : ""}`}>
        <p className="primary-text">Where in the world?</p>
        <div className="mode-change" onClick={themeChange}>
          <FontAwesomeIcon icon={faMoon} />
          <p className="secondary-text">
            {isDarkMode ? "Light Mode" : "Dark Mode"}
          </p>
        </div>
      </div>
    </>
  );
};

export default NavBar;
