/* eslint-disable react/prop-types */
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import FilterOptions from "./FilterOptions";

let SearchInput = ({
  handleSearchFilter,
  handleSelectedRegion,
  countries,
  isDarkMode,
}) => {
  return (
    <div className={`search-filter-container ${isDarkMode ? "main-dark" : ""}`}>
      <div className={`input-container ${isDarkMode ? "dark" : ""}`}>
        <FontAwesomeIcon icon={faMagnifyingGlass} className="search-icon" />
        <input
          type="text"
          defaultValue=""
          placeholder="Search for a country"
          className={`search-input ${isDarkMode ? "dark" : ""}`}
          onChange={(event) => {
            return handleSearchFilter(event.target.value.toLowerCase());
          }}
        />
      </div>
      <FilterOptions
        handleSelectedRegion={handleSelectedRegion}
        countries={countries}
        isDarkMode={isDarkMode}
      />
    </div>
  );
};

export default SearchInput;
