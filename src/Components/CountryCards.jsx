import { v4 as uuidv4 } from "uuid";
import { Link } from "react-router-dom";

let CountryCards = ({ countries, isDarkMode }) => {
  return countries.map((country) => {
    return (
      <>
        <div className={`card ${isDarkMode ? "dark" : ""}`} key={uuidv4()}>
          <Link to={`/country/${country.cca3}`} className="link">
            <img src={country.flags.svg} className="flag-img"></img>
            <div
              className={`country-details-container  ${
                isDarkMode ? "dark" : ""
              }`}
            >
              <p className="country-name-text">{country.name.common}</p>
              <div className={`country-details`}>
                <div className="detail">
                  <p>Population:</p>
                  <p>{country.population}</p>
                </div>
                <div className="detail">
                  <p>Region:</p>
                  <p>{country.region}</p>
                </div>
                <div className="detail">
                  <p>Capital:</p>
                  <p>{country.capital}</p>
                </div>
              </div>
            </div>
          </Link>
        </div>
      </>
    );
  });
};

export default CountryCards;
