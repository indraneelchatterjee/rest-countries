/* eslint-disable react/prop-types */
import { useEffect, useState } from "react";
import axios from "axios";
import { Link, useParams } from "react-router-dom";

let CountryDetails = ({ isDarkMode }) => {
  let { id } = useParams();
  let [country, setCountry] = useState(null);
  const [error, setError] = useState("");

  useEffect(() => {
    const getData = async () => {
      try {
        const response = await axios.get(
          `https://restcountries.com/v3.1/alpha/${id}`
        );
        const data = response.data;
        setCountry(data[0]);
      } catch (error) {
        setError(error.message);
      }
    };
    getData();
  }, [id]);

  let getRequiredData = (requiredObj) => {
    let val = [];
    let keys;
    if (requiredObj !== undefined) {
      keys = Object.keys(requiredObj);
    }
    if (keys !== undefined) {
      keys.map((key) => {
        let a = key;
        let check = requiredObj[a];
        check["name"]
          ? val.push(requiredObj[a]["name"])
          : val.push(requiredObj[a]);
      });
    } else {
      val.push("N.A.");
    }
    return val.join(",");
  };
  let dataFromArray = (arr) => {
    if (arr !== undefined) {
      if (arr.length > 1) {
        return arr.join(",");
      } else {
        return arr[0];
      }
    } else {
      return "N.A.";
    }
  };
  let getNativeName = (obj) => {
    if (obj !== undefined) {
      let keys = Object.keys(obj);
      return obj[keys[0]].common;
    } else {
      return "NA";
    }
  };
  return country ? (
    <>
      {error && <p className="error-text">{error}</p>}
      <div className={`${isDarkMode ? "main-dark" : ""} full`}>
        <div className={`align ${isDarkMode ? "main-dark" : ""}`}>
          <div className={`back-btn-container ${isDarkMode ? "dark" : ""}`}>
            <Link to={`/`}>
              <button className="back-btn">Back</button>
            </Link>
          </div>
        </div>
        <div
          className={`country-full-detail-container ${
            isDarkMode ? "main-dark" : ""
          }`}
        >
          <div className="image-container">
            <img src={country.flags.svg} className="full-details-img" />
          </div>

          <div className="full-details-container">
            <h1>{country.name.common}</h1>
            <div className="sub-details">
              <div className="details-col">
                <div className="detail">
                  <p>Native Name:</p>
                  <p>{getNativeName(country.name.nativeName)}</p>
                </div>
                <div className="detail">
                  <p>Population:</p>
                  <p>{country.population}</p>
                </div>
                <div className="detail">
                  <p>Region:</p>
                  <p>{country.region}</p>
                </div>
                <div className="detail">
                  <p>Sub Region:</p>
                  <p>{country.subregion}</p>
                </div>
                <div className="detail">
                  <p>Capital:</p>
                  <p>{dataFromArray(country.capital)}</p>
                </div>
              </div>
              <div className="details-col">
                <div className="detail">
                  <p>Top LevelDomain:</p>
                  <p>{dataFromArray(country.tld)}</p>
                </div>
                <div className="detail">
                  <p>Currencies:</p>
                  <p>{getRequiredData(country.currencies)}</p>
                </div>
                <div className="detail">
                  <p>Languages:</p>
                  <p>{getRequiredData(country.languages)}</p>
                </div>
              </div>
            </div>
            <div className="detail align-borders">
              <p>Border Countries:</p>
              <div className="each-country-border">
                {country.borders && country.borders.length > 0 ? (
                  country.borders.map((border) => (
                    <Link to={`/country/${border}`} key={border}>
                      <button className="border-btn">{border}</button>
                    </Link>
                  ))
                ) : (
                  <p>N.A.</p>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  ) : (
    <div className="loading-text">{error ? error : "Loading.."}</div>
  );
};

export default CountryDetails;
