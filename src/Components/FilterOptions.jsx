/* eslint-disable react/prop-types */
let FilterOptions = ({ handleSelectedRegion, countries, isDarkMode }) => {
  let regionArray = countries.reduce((acc, country) => {
    acc.add(country.region);
    return acc;
  }, new Set());
  return (
    <div className={`options-container ${isDarkMode ? "dark" : ""}`}>
      <select
        className={`filter-options ${isDarkMode ? "dark" : ""}`}
        onChange={(event) => handleSelectedRegion(event.target.value)}
      >
        <option value="" selected className={`${isDarkMode ? "dark" : ""}`}>
          Filter By Region
        </option>
        {[...regionArray].map((regionName) => (
          <option
            key={regionName}
            value={regionName}
            className={`${isDarkMode ? "dark" : ""}`}
          >
            {regionName}
          </option>
        ))}
      </select>
    </div>
  );
};

export default FilterOptions;
