/* eslint-disable react/prop-types */
import { useEffect, useState } from "react";
import axios from "axios";
import SearchInput from "./SearchInput";
import CountryCards from "./CountryCards";
const HomePage = ({ isDarkMode }) => {
  const [countries, setCountries] = useState([]);
  const [regionName, setRegionName] = useState("");
  const [searchValue, setSearchValue] = useState("");
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState("");

  useEffect(() => {
    getData();
  }, []);
  const getData = async () => {
    try {
      setLoading(true);
      const response = await axios.get("https://restcountries.com/v3.1/all");
      const data = response.data;
      setCountries(() => {
        return data;
      });
      setLoading(false);
    } catch (error) {
      setError(error.message);
      setLoading(false);
    }
  };

  const filteredCountries = countries.filter((country) => {
    return (
      country.name.common.toLowerCase().startsWith(searchValue.toLowerCase()) &&
      (regionName === "" || country.region === regionName)
    );
  });

  const handleSelectedRegion = (region) => {
    setRegionName(region);
  };
  const handleSearchFilter = (input) => {
    setSearchValue(input);
  };
  return (
    <>
      <div className={`${isDarkMode ? "main-dark" : ""}`}>
        <SearchInput
          handleSearchFilter={handleSearchFilter}
          handleSelectedRegion={handleSelectedRegion}
          countries={countries}
          isDarkMode={isDarkMode}
        />
        {filteredCountries.length === 0 && !isLoading && (
          <p className="loading-text"> No Record Found</p>
        )}
        {error && <p className="error-text">{error}</p>}
        {isLoading && <div className="loading-text">Loading...</div>}
        <div className="cards-container">
          <CountryCards countries={filteredCountries} isDarkMode={isDarkMode} />
        </div>
      </div>
    </>
  );
};

export default HomePage;
