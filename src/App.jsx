import { Route, Routes } from "react-router-dom";
import { useState } from "react";

import CountryDetails from "./Components/CountryDetails";
import HomePage from "./Components/HomePage";
import NavBar from "./Components/NavBar";

function App() {
  const [isDarkMode, setDarkMode] = useState(false);

  const themeChange = () => {
    setDarkMode(!isDarkMode);
  };
  return (
    <>
      <NavBar themeChange={themeChange} isDarkMode={isDarkMode} />
      <Routes>
        <Route
          exact
          path="/"
          element={<HomePage isDarkMode={isDarkMode} />}
        ></Route>
        <Route
          path="/country/:id"
          element={<CountryDetails isDarkMode={isDarkMode} />}
        />
      </Routes>
    </>
  );
}

export default App;
